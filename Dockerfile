FROM node:18.12.0
WORKDIR /test_docker
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "node", "bin/www" ]