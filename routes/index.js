var express = require('express');
var router = express.Router();
const modelEmp = require('../model/emp');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Docker From GitLab Push' });
});

router.get('/add', async function(req, res, next) {
  try {
    await modelEmp.create({
      firstName: 'John',
      lastName: 'Doe',
      email: 'inet@example.com',
      age: 25
    })
  .then(() => {
    res.send('Created Employee successfully');
  })
  .catch((error) => {
    throw (error);
  });
  } catch (error) {
  res.status(500).send(error)
  }
});


module.exports = router;
