const { Sequelize,DataTypes } = require('sequelize');

const db = 'mariacompose1';
const dbType = 'mariadb';
const username = 'root';
const password = 'root';
const localhost = process.env.HOST;
const porthost = '3306';
const sequelize = new Sequelize(db, username, password, {
    host: localhost,
    dialect: dbType,
    port: porthost,
  });

sequelize.authenticate()
.then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const modelEmp = sequelize.define('Employee',{
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      age: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
});

modelEmp.sync({alter:true});

module.exports = modelEmp;
